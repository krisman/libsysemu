/*
 * Copyright (C) 2020 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <syscall-emulate.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/unistd.h>

pid_t gettid(void)
{
	return syscall(SYS_gettid);
}

int main ()
{
	struct sysemu_context *ctx = sysemu_create_context();

	sysemu_whitelist_vdso(ctx);

	sysemu_whitelist_libc(ctx);

	sysemu_base_syscall_filter(ctx, 186);

	if (gettid() < 0)
		printf("gettid first try failed.\n");

	sysemu_install_filter(ctx);

	write(1, "could write ok\n", sizeof("could write ok\n"));

	if (gettid() < 0)
		printf("gettid second try failed. yey\n");


	sysemu_free_context(ctx);

	return 0;
}
