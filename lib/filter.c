/*
 * Copyright (C) 2020 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.com>
 */

#include "internal.h"

#include <linux/filter.h>
#include <stdint.h>
#include <linux/seccomp.h>
#include <sys/prctl.h>
#include <stdlib.h>
#include <stddef.h>
#include <err.h>

// #define DUMP_BPF

#define FILTER_MAX 512
struct filter {
	struct sock_fprog fprog;
	int offset;
	uint8_t jump_allow_target;
	uint8_t jump_reject_target;
	struct sock_filter filter[];
};

#define syscall_nr (offsetof(struct seccomp_data, nr))
#define arch_nr (offsetof(struct seccomp_data, arch))
#define ip_LW (offsetof(struct seccomp_data, instruction_pointer))
#define ip_HW (offsetof(struct seccomp_data, instruction_pointer) + 4)

int dry_run = 1;

#ifdef DUMP_BPF
#include <stdio.h>
#define DUMP(offset, x) fprintf(stderr, "%.4x: %s\n", offset, (x))
#else
#define DUMP(offset, x)
#endif

#define _(x)							\
	do {							\
		if (!dry_run) {					\
			DUMP(filter->offset, #x);		\
			filter->filter[filter->offset++] =	\
				((struct sock_filter) x);	\
		} else						\
			filter->offset++;			\
	} while (0)

#define REL_ADDR(x) ((x) - filter->offset)

static void generate_quick_bail(struct sysemu_context *ctx,
				struct filter *filter)
{
	/*
	 * Allow syscall immediately if it syscall number is lower than
	 * a given threshold.  The strategy is that existing
	 * syscalls that go through the WinAPI will have a small number,
	 * so we can avoid look up the mappings.
	 */

	_(BPF_STMT(BPF_LD | BPF_W | BPF_ABS, syscall_nr));
	_(BPF_JUMP(BPF_JMP | BPF_JGT | BPF_K,
		   ctx->syscall_threshold,
		   0, REL_ADDR(filter->jump_allow_target)));
}

static void generate_segment_rule(struct sysemu_context *ctx,
				  struct filter *filter,
				  struct mem_segment *map)
{
	uint32_t SH, SL;
	uint32_t EH, EL;
	int next;

	SL = (uint32_t) map->start;
	SH = (uint32_t) (map->start >> 32);
	EL = (uint32_t) map->end;
	EH = (uint32_t) (map->end >> 32);

	if (EH != SH)
		err(1, "Mapping cross 32-bit boundary\n");

	/* pseudo-code of bpf below:
	  if (EH == IP_H) {
		if (L > EL)
			goto next;
		if (L >= SL)
			accept;
	  }
	  else {
		goto next;
	 }
	   next:
	*/
	next = 5 + filter->offset;

	_(BPF_STMT(BPF_LD | BPF_W | BPF_ABS, ip_HW));
	_(BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, EH, 0, REL_ADDR(next)));

	_(BPF_STMT(BPF_LD | BPF_W | BPF_ABS, ip_LW));
	_(BPF_JUMP(BPF_JMP | BPF_JGT | BPF_K, EL, REL_ADDR(next), 0));
	_(BPF_JUMP(BPF_JMP | BPF_JGE | BPF_K, SL,
		   REL_ADDR(filter->jump_allow_target),
		   REL_ADDR(next)));

#ifdef DUMP_BPF
	DUMP(next, "next:");
#endif

}

static void generate_exit_points(struct sysemu_context *ctx,
				 struct filter *filter)
{
	_(BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_TRAP));
	_(BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_ALLOW));
}

static void dump_label_table(struct filter *filter)
{
#ifdef DUMP_BPF
	DUMP(-1, "\n  Jump table:");
	DUMP(filter->jump_reject_target, "jump_reject_target");
	DUMP(filter->jump_allow_target, "jump_allow_target");
#endif
}

static struct sock_fprog *compile_filter(struct sysemu_context *ctx)
{
	struct filter *filter = malloc(sizeof(struct filter) + FILTER_MAX);
	struct mem_segment *mapping;
	int i;

	filter->fprog.filter = &filter->filter[0];
	filter->offset = 0;

	/*
	 * Perform 2 passes.  The first a dry run to calculate jump
	 * targets, the second to actually generate the program.
	 */
	for (dry_run = 1, i = 0; i < 2 ; i++) {
		if (ctx->syscall_threshold > 0)
			generate_quick_bail(ctx, filter);

		for_each_whitelisted_segment(mapping, ctx)
			generate_segment_rule(ctx, filter, mapping);

		/*
		 * calculate jump table on first pass, and generate exit
		 * points on second pass.
		 */
		if (dry_run) {
			filter->jump_reject_target = filter->offset++;
			filter->jump_allow_target = filter->offset;

			if (filter->offset > FILTER_MAX) {
				err(1, "filter too large. Reduce constraints. "
				    "Cannot generate\n");
			}

			/*
			 * Prepare for second iteration. Write bytecode
			 * for real.
			 */
			filter->offset = 0;
			dry_run = 0;
		} else {
			generate_exit_points(ctx, filter);
		}
	}

	dump_label_table(filter);

	filter->fprog.len = filter->offset;

	return &filter->fprog;
}

EXTERNAL_API
int sysemu_install_filter(struct sysemu_context *ctx)
{
	struct sock_fprog *fprog = compile_filter(ctx);

	if (!fprog)
		err(1, "Failed to compile filter");

	if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0))
		err(1, "Failed to set no new privs");

	if (prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, fprog, 0, 0))
		err(1, "failed seccomp");

	return 0;
}

EXTERNAL_API
void sysemu_base_syscall_filter(struct sysemu_context *ctx, int sys_nr)
{
	ctx->syscall_threshold = sys_nr;
}
