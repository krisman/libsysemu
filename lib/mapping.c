/*
 * Copyright (C) 2020 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.com>
 */

#include "internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <string.h>

struct object {
	char *path;
	int inode;
	struct object *next;
};


#define PROCFS_MAP_X_BIT_OFF 28
#define PROCFS_INODE_OFF 45
#define PROCFS_NAME_OFF 73

struct object *get_object(struct sysemu_context *ctx, unsigned long inode,
			  const char *path)
{
	struct object *obj;
	for (obj = ctx->objects ; obj ; obj = obj->next) {
		if ((inode && obj->inode == inode) ||
		    !strcmp(obj->path, path))
			return obj;
	}

	return NULL;
}

static struct object *get_or_create_object(struct sysemu_context *ctx,
					   unsigned long inode,
					   const char *path)
{
	struct object *obj = get_object(ctx, inode, path);
	if (obj)
		return obj;

	obj = malloc(sizeof(struct object));
	if (!obj)
		err(1, "%s ENOMEM\n", __FUNCTION__);

	obj->path = strdup(path);
	obj->inode = inode;

	obj->next = ctx->objects;
	ctx->objects = obj;

	return obj;
}

void segment_insert_sorted(struct mem_segment **head, struct mem_segment *seg)
{
	for (; *head && (*head)->start > seg->end; head = &((*head)->next));

	seg->next = *head;
	*head = seg;
}

static void whitelist_insert_sorted(struct mem_segment **head,
				    struct mem_segment *seg)
{
	for (; *head && (*head)->start > seg->end;
	     head = &((*head)->whitelist));

	seg->whitelist = *head;
	*head = seg;
}

EXTERNAL_API
int sysemu_whitelist_vdso(struct sysemu_context *ctx)
{
	struct mem_segment *seg;
	int cnt = 0;

	for_each_segment(seg, ctx) {
		if (seg->parent->inode ||
		    strcmp("[vdso]\n", seg->parent->path))
			continue;

		whitelist_insert_sorted(&ctx->whitelist, seg);

		cnt++;
	}

	return cnt;
}

EXTERNAL_API
int sysemu_whitelist_libc(struct sysemu_context *ctx)
{
	struct object *libc_object, *loader_object;
	struct mem_segment *seg;
	int cnt = 0;

	libc_object = get_object(ctx, 0, "/lib/x86_64-linux-gnu/libc-2.30.so\n");
	loader_object = get_object(ctx, 0, "/lib/x86_64-linux-gnu/ld-2.30.so\n");

	if (!libc_object || !loader_object)
		return -1;

	for_each_segment(seg, ctx) {
		if (seg->parent != libc_object && seg->parent != loader_object)
			continue;

		if (!seg->exec)
			continue;

		whitelist_insert_sorted(&ctx->whitelist, seg);

		cnt++;
	}

	return cnt;
}

static int load_procfs_segments(struct sysemu_context *ctx)
{
	FILE *f;
	char *maps = "/proc/self/maps";
	char line[1024];
	int r;
	unsigned long inode;
	struct mem_segment *seg;


	f = fopen(maps, "r");
	if (!f)
		err(1, "Can't open /proc/self/maps");

	while (fgets(line, 1024, f)) {

		seg = malloc(sizeof(struct mem_segment));
		if (!seg)
			err(1, "%s ENOMEM\n", __FUNCTION__);

		r = sscanf(line, "%lx-%lx", &seg->start, &seg->end);
		if (r < 2)
			err(1, "line range parsing failed %d\n", r);

		seg->exec = (line[PROCFS_MAP_X_BIT_OFF] == 'x');

		r = sscanf(line+PROCFS_INODE_OFF, "%lu", &inode);
		if (r < 1)
			err(1, "line inode parsing failed %d\n", r);

		seg->parent = get_or_create_object(ctx, inode,
						   line+PROCFS_NAME_OFF);

		segment_insert_sorted(&(ctx->segments), seg);

	}

	return 0;
}

EXTERNAL_API
struct sysemu_context *sysemu_create_context()
{
	struct sysemu_context *ctx = malloc(sizeof(struct sysemu_context));

	if (!ctx)
		err(1, "%s ENOMEM\n", __FUNCTION__);
	memset (ctx, '\0', sizeof(*ctx));

	load_procfs_segments(ctx);

	return ctx;
}

EXTERNAL_API
void sysemu_free_context(struct sysemu_context *ctx)
{
	struct object *obj, *prev_obj;
	struct mem_segment *seg, *prev_seg;

	obj = ctx->objects;
	while (obj) {
		prev_obj = obj;
		obj = obj->next;
		free (prev_obj);
	}

	seg = ctx->segments;
	while (seg) {
		prev_seg = seg;
		seg = seg->next;
		free (prev_seg);
	}

	free(ctx);
}

