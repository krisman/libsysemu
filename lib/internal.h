/*
 * Copyright (C) 2020 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.com>
 */

#ifndef _SYSEMU_INTERNAL_H
#define _SYSEMU_INTERNAL_H

#include <stdint.h>

#define EXTERNAL_API __attribute ((visibility("default")))

struct sysemu_context {
	struct object *objects;
	struct mem_segment *segments;
	struct mem_segment *whitelist;
	int syscall_threshold;
};

struct object;
struct mem_segment {
	uint64_t start;
	uint64_t end;
	int exec:1;
	struct object *parent;

	struct mem_segment *next;
	struct mem_segment *whitelist;
};

#define for_each_segment(entry, ctx) \
	for (entry = ctx->segments; entry ; entry = entry->next)

#define for_each_whitelisted_segment(entry, ctx) \
	for (entry = ctx->whitelist; entry ; entry = entry->whitelist)
#endif
