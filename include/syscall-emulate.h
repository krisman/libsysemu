/*
 * Copyright (C) 2020 Collabora Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Gabriel Krisman Bertazi <krisman@collabora.com>
 */

#ifndef __SYSCALL_EMULATE_H
#define __SYSCALL_EMULATE_H

struct sysemu_context;

struct sysemu_context *sysemu_create_context();
void sysemu_free_context(struct sysemu_context *ctx);

void sysemu_whitelist_vdso(struct sysemu_context *ctx);
void sysemu_whitelist_libc(struct sysemu_context *ctx);

void sysemu_base_syscall_filter(struct sysemu_context *ctx, int sys_nr);

int sysemu_install_filter(struct sysemu_context *ctx);

#endif
